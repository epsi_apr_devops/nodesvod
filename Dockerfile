FROM node:latest
WORKDIR /app
COPY . .
RUN npm ci
RUN ./node_modules/.bin/eslint app.js
CMD node server.js